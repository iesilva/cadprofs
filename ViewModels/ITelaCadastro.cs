﻿using Dominio.Models;

namespace CadastroProfessores.ViewModels
{
    public interface ITelaCadastro<T> where T : Model, new()
    {
        void Abrir(BaseViewModel<T> viewModel);
        void Fechar();
    }
}