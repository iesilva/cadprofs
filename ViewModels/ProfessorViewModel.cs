﻿using Dominio.DAOs;
using Dominio.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace CadastroProfessores.ViewModels
{
    class ProfessorViewModel : BaseViewModel<Professor>
    {
        public ProfessorViewModel(ITelaCadastro<Professor> telaCadastro) : base(telaCadastro)
        {
            PreencherLista();
        }

        protected override IDAO<Professor> GetDAO()
        {
            return dao;
        }

        private ProfessorDAO dao = new ProfessorDAO();
    }
}
