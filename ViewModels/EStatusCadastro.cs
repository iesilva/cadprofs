﻿namespace CadastroProfessores.ViewModels
{
    public enum EStatusCadastro
    {
        Listando = 0,
        Consultando = 1,
        Incluindo = 2,
        Alterando = 3,
    }
}