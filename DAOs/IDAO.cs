﻿using Dominio.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.DAOs
{
    public interface IDAO<T> where T : Model
    {
        void insert(T obj);
        void update(T obj);
        void delete(T obj);
        List<T> findAll();
        T findById(string key);
    }
}
