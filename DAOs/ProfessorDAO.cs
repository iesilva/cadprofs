﻿using Dominio.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dominio.DAOs
{
    public class ProfessorDAO : BaseDAO<Professor>, IDAO<Professor>
    {
        protected override string GetSqlDelete() =>
             "DELETE FROM Professor WHERE ID=@ID";

        protected override string GetSqlInsert() =>
            "INSERT INTO Professor (NOME, MATRICULA) VALUES (@NOME, @MATRICULA)";

        protected override string GetSqlSelect() =>
            "SELECT * FROM Professor ORDER BY NOME";

        protected override string GetSqlSelectId() =>
            "SELECT * FROM Professor WHERE ID=@ID";

        protected override string GetSqlUpdate() =>
            "UPDATE Professor SET NOME=@NOME, MATRICULA=@MATRICULA WHERE ID=@ID";

        protected override void AdicionarParametrosExcetoId(SqlCommand cmd, Professor obj)
        {
            cmd.Parameters.AddWithValue("@NOME", obj.Nome);
            cmd.Parameters.AddWithValue("@MATRICULA", obj.Matricula);
        }

        protected override Professor GetObjeto(DataRow reg)
        {
            var obj = new Professor();

            obj.id = reg["ID"].ToString();
            obj.Nome = reg["NOME"].ToString();
            obj.Matricula = Convert.ToInt32(reg["MATRICULA"]);

            return obj;
        }
    }
}
