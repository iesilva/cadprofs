﻿using CadastroProfessores.ViewModels;
using Dominio.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CadastroProfessores.Views
{
    /// <summary>
    /// Interaction logic for ProfessoresListView.xaml
    /// </summary>
    public partial class ProfessoresListView : Window
    {
        public ProfessoresListView()
        {
            InitializeComponent();
            DataContext = new ProfessorViewModel(new TelaCadastroApresentacao<Professor, ProfessorView>());
        }
    }
}
