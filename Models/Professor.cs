﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Models
{
    public class Professor : Model
    {
        public string Nome { get; set; }
        public int Matricula { get; set; }
    }
}
